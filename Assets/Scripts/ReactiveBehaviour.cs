using UniRx;
using UnityEngine;

public class ReactiveBehaviour : MonoBehaviour
{
    protected CompositeDisposable Disposables;

    private void OnEnable()
    {
        Disposables = new CompositeDisposable();
    }

    private void OnDisable()
    {
        Disposables.Clear();
    }
}