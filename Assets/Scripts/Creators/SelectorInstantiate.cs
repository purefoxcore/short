using Zenject;

public class SelectorInstantiate : IInitializable
{
    private readonly Selector.Factory _factory;

    public SelectorInstantiate(Selector.Factory factory) =>
        _factory = factory;

    public void Initialize() => 
        _factory.Create();
}