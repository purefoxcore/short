using Zenject;
using UnityEngine;

public class ColorShortInstantiate : IInitializable
{
    private readonly ColorShort.Factory _factory;
    private readonly int _count;
    private readonly Vector3 _minBound;
    private readonly Vector3 _maxBound;

    public ColorShortInstantiate(ColorShort.Factory factory, int count, Vector3 minBound, Vector3 maxBound)
    {
        _factory = factory;
        _count = count;
        _minBound = minBound;
        _maxBound = maxBound;
    }

    public void Initialize()
    {
        for (int i = 0; i < _count; i++)
        {
            ColorShort colorShort = _factory.Create();

            colorShort.transform.position = new Vector3(
                Random.Range(_minBound.x, _maxBound.x),
                Random.Range(_minBound.y, _maxBound.y),
                Random.Range(_minBound.z, _maxBound.z));

            colorShort.Color = EnumExtensions.RandomEnumValue<EntityColor>();
        }
    }
}