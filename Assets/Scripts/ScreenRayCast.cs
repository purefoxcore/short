using UnityEngine;

[RequireComponent(typeof(Camera))]
public class ScreenRayCast : MonoBehaviour
{
    [SerializeField] [HideInInspector] private Camera _camera;

    private void OnValidate()
    {
        _camera = GetComponent<Camera>();
    }

    public RaycastHit CastRay(Vector3 position)
    {
        var ray = _camera.ScreenPointToRay(position);
        Physics.Raycast(ray, out var hit);
        return hit;
    }
}