using System;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ColorCheck : MonoBehaviour
{
    [SerializeField] private EntityColor _currentColor;
    private event Action _colorMatch;
    
    public event Action ColorMatch
    {
        add => _colorMatch += value;
        remove => _colorMatch -= value;
    }

    private void OnValidate() => GetComponent<Collider>().isTrigger = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out ColorShort colorShort) && colorShort.Matches(_currentColor))
        {
            if (other.TryGetComponent(out PulseController pulseController)) 
                pulseController.Pulse();

            if (other.TryGetComponent(out NavmeshMovable movable)) 
                movable.Stop();

            if (other.TryGetComponent(out Selectable selectable)) 
                Destroy(selectable);
            
            _colorMatch?.Invoke();
        }
    }
}