using UniRx;
using Zenject;
using UnityEngine;
using UniRx.Triggers;

public class Selector : ReactiveBehaviour
{
    [Inject] private ScreenRayCast _screenRayCast;
    private ISelectable _selected;
    private IMovable _movable;

    private void Start()
    {
        this.UpdateAsObservable()
            .Where(_ => Input.touchCount == 1)
            .Select(_ => Input.GetTouch(0))
            .Where(touch => touch.phase == TouchPhase.Began)
            .Subscribe(OnTouch)
            .AddTo(Disposables);
    }

    private void OnTouch(Touch touch)
    {
        var hit = _screenRayCast.CastRay(touch.position);

        if (_selected == null)
            Select(hit);
        else
            MoveSelected(hit);
    }

    private void Select(RaycastHit hit)
    {
        if (hit.transform.TryGetComponent(out ISelectable selectable))
        {
            selectable.Select();
            _selected = selectable;
        }

        if (hit.transform.TryGetComponent(out IMovable movable))
            _movable = movable;
    }

    private void MoveSelected(RaycastHit hit)
    {
        _movable?.MoveTo(hit.point);
        _selected?.DeSelect();
        _selected = null;
        _movable = null;
    }

    public class Factory : PlaceholderFactory<Selector>
    {
    }
}