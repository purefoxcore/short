using UnityEngine;

[RequireComponent(typeof(Outline))]
public class Selectable : MonoBehaviour, ISelectable
{
    [HideInInspector] [SerializeField] private Outline _outline;

    private void OnValidate() => _outline = GetComponent<Outline>();
    private void OnDisable() => DeSelect();
    
    public void Select() => _outline.enabled = true;
    public void DeSelect() => _outline.enabled = false;
}