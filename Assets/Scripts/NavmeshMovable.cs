using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class NavmeshMovable : MonoBehaviour, IMovable
{
    [HideInInspector] [SerializeField] private NavMeshAgent _agent;

    private void OnValidate() => _agent = GetComponent<NavMeshAgent>();
    public void MoveTo(Vector3 position) => _agent.SetDestination(position);
    public void Stop() => _agent.isStopped = true;
}