using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Slider))]
public class CheckBasedSliderChanger : MonoBehaviour
{
    [SerializeField] private List<ColorCheck> _colorChecks;

    [Inject] private int _colorShortCount;
    private Slider _slider;
    private float _count;

    private void OnEnable()
    {
        foreach (var colorCheck in _colorChecks)
            colorCheck.ColorMatch += OnColorMatch;
    }

    private void OnDisable()
    {
        foreach (var colorCheck in _colorChecks)
            colorCheck.ColorMatch -= OnColorMatch;
    }

    private void OnColorMatch()
    {
        _count++;
        _slider.value = _count / _colorShortCount;
    }

    private void OnValidate()
    {
        _slider = GetComponent<Slider>();
    }
}