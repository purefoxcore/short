using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RigidBodyMovable : MonoBehaviour, IMovable
{
    [HideInInspector] [SerializeField] private Rigidbody _rigidbody;

    private void OnValidate() => _rigidbody = GetComponent<Rigidbody>();
    
    // TODO: Fix
    public void MoveTo(Vector3 position) => _rigidbody.MovePosition(position);
}