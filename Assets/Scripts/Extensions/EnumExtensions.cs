using System;
using System.Collections;

public static class EnumExtensions
{
    private static readonly Random _random = new Random();

    public static T RandomEnumValue<T>()
    {
        IList values = Enum.GetValues(typeof(T));
        int random = _random.Next(0, values.Count);

        return (T) values[random];

        // int random = UnityEngine.Random.Range(0, values.Length);
        // return (T) values.GetValue(random);
    }
}