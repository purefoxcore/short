using Zenject;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class ColorShortInstaller : MonoInstaller
{
    private BoxCollider _collider;
    [SerializeField] private int _count = 1;
    [SerializeField] private ColorShort _colorShort;
    [SerializeField] private CheckBasedSliderChanger _ui;

    private void OnValidate() =>
        _collider = GetComponent<BoxCollider>();

    public override void InstallBindings()
    {
        Container
            .BindFactory<ColorShort, ColorShort.Factory>()
            .FromComponentInNewPrefab(_colorShort)
            .AsSingle();

        Container
            .BindInterfacesTo<ColorShortInstantiate>()
            .AsSingle()
            .WithArguments(_count, transform.position - _collider.size / 2, transform.position + _collider.size / 2);

        Container
            .Bind<int>()
            .FromInstance(_count)
            .AsSingle();
        
        Container
            .Bind<CheckBasedSliderChanger>()
            .FromInstance(_ui)
            .AsSingle();
    }
}