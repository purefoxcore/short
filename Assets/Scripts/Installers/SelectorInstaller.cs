using Zenject;
using UnityEngine;

public class SelectorInstaller : MonoInstaller
{
    [SerializeField] private Selector _selector;
    [SerializeField] private ScreenRayCast _screenRayCast;

    public override void InstallBindings()
    {
        Container
            .Bind<ScreenRayCast>()
            .FromInstance(_screenRayCast)
            .AsSingle();

        Container
            .BindFactory<Selector, Selector.Factory>()
            .FromComponentInNewPrefab(_selector)
            .AsSingle();

        Container
            .BindInterfacesAndSelfTo<SelectorInstantiate>()
            .AsSingle();
    }
}