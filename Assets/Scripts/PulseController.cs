using UnityEngine;
using System.Collections;

public class PulseController : MonoBehaviour
{
    [SerializeField] private float _lifeTime;

    private Renderer[] _renderers;
    private static readonly int _tintAmount = Shader.PropertyToID("Vector1_39d7c6622dd34500acd0d32097231baf");

    private void Awake()
    {
        _renderers = GetComponentsInChildren<Renderer>();
    }

    public void Pulse() => StartCoroutine(DoPulse());

    private IEnumerator DoPulse()
    {
        float time = _lifeTime;
        while (time >= 0)
        {
            foreach (var currentRenderer in _renderers)
                currentRenderer.material.SetFloat(_tintAmount, Mathf.Lerp(0f, 1f, time / _lifeTime));

            time -= Time.deltaTime;
            yield return null;
        }
    }
}