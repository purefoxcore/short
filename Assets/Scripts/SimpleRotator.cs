using UnityEngine;

public class SimpleRotator : MonoBehaviour
{
    [SerializeField] private float _rotateSpeed;
    [SerializeField] private Vector3 _euler;

    private void OnValidate()
    {
        _euler = new Vector3(Mathf.Clamp01(_euler.x), Mathf.Clamp01(_euler.y), Mathf.Clamp01(_euler.z));
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + _euler);
    }

    private void Update()
    {
        transform.Rotate(_euler * _rotateSpeed * Time.deltaTime);
    }
}