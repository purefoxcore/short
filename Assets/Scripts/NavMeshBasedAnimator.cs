using UniRx;
using UnityEngine;
using UnityEngine.AI;
using UniRx.Triggers;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
public class NavMeshBasedAnimator : ReactiveBehaviour
{
    [HideInInspector] [SerializeField] private NavMeshAgent _agent;
    [HideInInspector] [SerializeField] private Animator _animator;
    [SerializeField] private string _walkingAnimationKey;

    private void OnValidate()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        this.UpdateAsObservable()
            .Select(_ => _agent.velocity.magnitude)
            .Subscribe(magnitude => _animator.SetFloat(_walkingAnimationKey, magnitude))
            .AddTo(Disposables);
    }
}