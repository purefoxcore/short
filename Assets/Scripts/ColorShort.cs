using System;
using Zenject;
using UnityEngine;

public class ColorShort : MonoBehaviour
{
    [SerializeField] private Material _redMaterial;
    [SerializeField] private Material _blueMaterial;
    [SerializeField] private Material _greenMaterial;

    private Renderer[] _renderers;
    private EntityColor _color;

    public EntityColor Color
    {
        get => _color;
        set
        {
            foreach (var currentRenderer in _renderers)
            {
                currentRenderer.material = value switch
                {
                    EntityColor.red => _redMaterial,
                    EntityColor.blue => _blueMaterial,
                    EntityColor.green => _greenMaterial,
                    _ => throw new ArgumentOutOfRangeException()
                };
            }

            _color = value;
        }
    }

    private void Awake()
    {
        _renderers = GetComponentsInChildren<Renderer>();
    }

    public bool Matches(EntityColor entityColor) => _color == entityColor;

    public class Factory : PlaceholderFactory<ColorShort>
    {
    }
}